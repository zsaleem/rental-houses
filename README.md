## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. cd into `rental-houses` directory.
    3. Run `yarn/npm install` command to download and install all dependencies.
    4. To run this project use `yarn/npm dev` command in command line.
    5. To build the project for production run `yarn/npm build` command. This will build the app for production and put all the files in `/build` folder.
    6. To run production ready version of the app run `yarn start` command and go to `http://localhost:8080`.

## Table of contents from [wiki page](https://gitlab.com/zsaleem/rental-houses/-/wikis/home)

1. [Home](https://gitlab.com/zsaleem/rental-houses/-/wikis/home)
2. [Getting Started](https://gitlab.com/zsaleem/rental-houses/-/wikis/getting-started)
3. [Development Process](https://gitlab.com/zsaleem/rental-houses/-/wikis/Development-process)
4. [Development Environment](https://gitlab.com/zsaleem/rental-houses/-/wikis/Development-Environment)
5. [DevOps](https://gitlab.com/zsaleem/rental-houses/-/wikis/DevOps)
6. [Architecture](https://gitlab.com/zsaleem/rental-houses/-/wikis/Architecture)

## Live Demo
To view live demo please [click here](https://rental-houses.herokuapp.com/).

To view list of CI/CD pipeline [click here](https://gitlab.com/zsaleem/rental-houses/-/pipelines).

To view list of all commits [click here](https://gitlab.com/zsaleem/rental-houses/-/commits/master).

To view list of all branches [click here](https://gitlab.com/zsaleem/rental-houses/-/branches/all).

To view list of all merge request [click here](https://gitlab.com/zsaleem/rental-houses/-/merge_requests?scope=all&state=all).

To view git branch strategy [click here](https://swimlanes.io/u/dsfhjcTkW).
