import { useState, useEffect } from 'react';
import axios from 'axios';

const useFetcher = () => {
	const [houses, setHouses] = useState();
	const [loader, setLoader] = useState(false);

	useEffect(() => {
		async function fetchHouses() {
			setLoader(true);
			const response = await axios.get(process.env.REACT_APP_URL);
			setHouses(response.data);
			setLoader(false);
		}

		fetchHouses();
	}, []);

	return {
		loader,
		houses,
	};
};

export { useFetcher };
