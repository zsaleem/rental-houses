import Typography from '@mui/material/Typography';

const Heading = ({
	variant,
	component,
	text,
}) => {
	return (
		<Typography gutterBottom variant={variant} component={component}>
      {text}
    </Typography>
	);
}

export { Heading };
