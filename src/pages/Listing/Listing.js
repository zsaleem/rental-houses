import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Badge from '@mui/material/Badge';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useStyles } from './Listing.styled';

const AVAILABLE = 'available';
const UNAVAILABLE = 'unavailable';
const BOOKED = 'available';

const Listing = ({ houses }) => {
	const small = useMediaQuery('(max-width:750px)');
	const classes = useStyles();

	return (
		<>
			{
				houses?.length > 0 && houses.map(house => (
					<Grid item xs={small ? 12 : 3} key={`${house.id}-${house.name}`} className={classes.position}>
						{
							<Badge
	      				badgeContent={
	      					house.bookable ?
	      					AVAILABLE :
	      						house.booked > 0 ?
	      							BOOKED :
	      							UNAVAILABLE
	      				}
	      				color={
	      					house.bookable ?
	      					'success' :
	      						house.booked > 0 ?
	      						'primary' :
	      						'error'
	      				}
	      				className={classes.status}>
	      			</Badge>
						}
						<Card
							sx={{ maxWidth: 345 }}
							className={small ? classes.responsive : classes.normal}>
				      <CardMedia
				        component="img"
				        alt={house.name}
				        height="140"
				        image={house.image}
				      />
				      <CardContent>
				      	<Typography variant="body2" color="text.secondary" className={classes.cardSecondaryHeader}>
				          id: {house.id}
				        </Typography>
				        <Typography gutterBottom variant="h5" component="div" className={classes.cardHeader}>
				          {house.name}
				        </Typography>
				      </CardContent>
				      <CardActions>
				      	{
				      		house.bookable ? (
				      			<Button
				      				variant="contained"
				      				className={classes.btn}
				      				disableElevation>
				      					Book
				      			</Button>
				      		) : house.booked > 0 ? (
							      		<Typography
							      			gutterBottom
							      			variant="span"
							      			component="span"
							      			className={classes.bookedText}>
							          		<CheckCircleOutlineIcon
							          			sx={{ fontSize: 16 }}
							          			className={classes.bookedIcon}
							          		/> Booked for {house.booked} days
							        	</Typography>
						        	) : (
					      				<Button
					      					variant="contained"
					      					disabled
					      					className={classes.btn}
					      					disableElevation>
					      						Not Bookable
					      				</Button>
				      				)
						    }
				      </CardActions>
				    </Card>
			    </Grid>
				))
			}
		</>
	);
}

export { Listing };
