import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles({
	responsive: {
		maxWidth: '100%',
		cursor: 'pointer',
		boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.05)',
		borderRadius: '5px',
		'&:hover': {
			boxShadow: '0px 10px 20px rgba(0, 0, 0, 0.15)',
			border: '1px solid #333333',
		}
	},
	normal: {
		cursor: 'pointer',
		boxShadow: '0px 3px 5px rgba(0, 0, 0, 0.05)',
		borderRadius: '5px',
		'&:hover': {
			boxShadow: '0px 10px 20px rgba(0, 0, 0, 0.15)',
			border: '1px solid #333333',
		}
	},
	position: {
		position: 'relative',
	},
	cardHeader: {
		color: '#333333',
		fontSize: '1rem',
		fontStyle: 'italic',
		fontWeight: '900',
		lineHeight: '20px',
	},
	cardSecondaryHeader: {
		fontSize: '0.875rem',
	},
	bookedText: {
		color: '#11A960',
		fontSize: '0.875rem',
		lineHeight: '1.1rem',
	},
	btn: {
		width: '100%',
		textTransform: 'capitalize',
		fontWeight: '900',
		fontSize: '0.875rem',
	},
	status: {
		position: 'absolute',
		left: '45px',
    top: '35px',
    padding: '2px 10px',
    textTransform: 'lowercase',
    borderRadius: '30px',
	},
	bookedIcon: {
		position: 'relative',
		top: '3px',
	}
});
