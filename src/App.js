import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import { Loader } from './components/Loader';
import { Heading } from './components/Heading';
import { Listing } from './pages/Listing';
import { useFetcher } from './hooks/useFetcher';

function App() {
	const { loader, houses } = useFetcher();

  return (
  	<Container maxWidth="lg">
  		<Heading variant="h1" component="h1" text="Rental Houses" />
			{
				loader ? (
					<Loader />
				) : (
					<Grid container spacing={2}>
			      <Listing houses={houses} />
			    </Grid>
				)
			}
    </Container>
  );
}

export default App;
